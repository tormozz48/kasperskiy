const url = require('url');
const nodemailer = require('nodemailer');
const { registerConfirmation } = require('../api/utils/mailHelper');
const { mail, host, port } = require('./vars');

const sendMessage = async (to, subject, content) => {
  const transporter = nodemailer.createTransport({
    from: mail.from,
    streamTransport: true,
    buffer: true,
  });

  const message = await transporter.sendMail({ to, subject, html: content });
  return message;
};

const sendConfirmationMessage = async (user) => {
  const {
    _id, email, name, locale,
  } = user;

  const baseUrl = url.format({
    hostname: host,
    port,
    pathname: '/v1/auth/confirm',
  });

  let message;

  try {
    const subject = registerConfirmation.getSubject(locale);
    const content = registerConfirmation.getContent(locale, {
      baseUrl, name, email, userId: _id,
    });

    message = await sendMessage(email, subject, content);
  } catch (error) {
    console.warn(error.message);
  }

  return message;
};

exports.sendMessage = sendMessage;
exports.sendConfirmationMessage = sendConfirmationMessage;
