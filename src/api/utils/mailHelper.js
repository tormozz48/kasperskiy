const _ = require('lodash');

exports.registerConfirmation = {
  template: `
    <h2><%= greeting %> <%= name %></h2>
    <p><%= description %> <%= email %></p>
    <p>
      <%= confirm %>
      <form method="post" action=<%= baseUrl %>>
        <input type="hidden" name="id" value=<%= userId %>/>
        <input type="submit" value=<%= submit %>/>
      </form>
    </p>
  `,
  locales: {
    en: {
      subject: 'Register confirmation',
      greeting: 'Hello',
      description: 'You registered user with email:',
      confirm: 'Please confirm your registration',
      submit: 'here',
    },
    ru: {
      subject: 'Подтверждение регистрации',
      greeting: 'Привет',
      description: 'Вы зарегистрировали пользователя с адресом:',
      confirm: 'Пожалуйста подтвердите регистрацию',
      submit: 'здесь',
    },
  },
  getSubject(locale) {
    return this.locales[locale].subject;
  },
  getContent(locale, data) {
    const localizedData = _.extend({}, this.locales[locale], data);
    return _.template(this.template)(localizedData);
  },
};
